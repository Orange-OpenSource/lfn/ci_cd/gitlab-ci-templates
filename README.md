# A list of gitlab-ci-templates

## Common variables

- `DOCKER_DRIVER`: which driver to use with docker in docker. Default to
  `overlay`.
- `JOBS_DISABLED`: you may want to __not__ trigger the job (for example
  scheduled tasks that do not need all the jobs). Setting the variable to a
  value will disable the step.

## doc8 template

check the `rst` with doc8 (using `testthedocs/ttd-doc8` container).

Template name: `doc8.gitlab-ci.yml`.

Variables:

- `DOC_PATH`: where to look at the doc (default `` so doc lays at the root of
    the repository)
- `DOC8_OPT`: options to pass to doc8 (default ``)

## pyup template

Look at the code of the default branch and generate Merge Requests if dependency
scanning shows outdated versions (see pyup.io to look at it).

Requirements:

- you must set `PYUP_ENABLED` to `True` to launch it (we recommends `daily`
  schedule);
- you _should_ have a `.pyup.yml` file to configure pyup

## spell-checking template

This templates needs to have a configuration file (`.vale.ini`) and spelling
files (set by configuration file).
You can take a look at this particular repository if you want to see a working
example.
By default, it will check everywhere for "*.md" files.
Variables:

- `SPELLING_PATH_EXCLUSION` : expression for certain folders exclusion.
- `MARKDOWN_ENABLED`: "Boolean" for enable / disable Markdown. If anything else
  than `True`, seen as `False`. `True` per default.
- `RST_ENABLED`: "Boolean" for enable / disable ReStructuredText. If anything
  else than `True`, seen as `False`. `False` per default.

## sphinx-pages template

Generate HTML page via sphinx and push them for the static website (see
<https://docs.gitlab.com/ee/user/project/pages/>).

Template name: `sphinx-pages.gitlab-ci.yml`.

Variables:

- `DOC_PATH`: where to look at the doc (default `` so doc lays at the root of
    the repository)

Requirements:

- you must have setup sphinx properly (`conf.py` and `Makefile` exists in
  `DOC_PATH`);
- sphinx plugins requirements are put in `doc-requirements.txt` at root of the
  project;
- you have a Python project to be installed (so we'll do `pip install .` in
  order to gather the coded modules) if you have a requirements.txt file.

## yaml template

check `yaml` files with `yamllint` ().

Template name: `yaml.gitlab-ci.yml`.

Variables:

- `YAML_PATH_EXCLUSION` : expression in order to exclude certain folders.

## git max commits

check that you do not add more than `GIT_MAX_COMMIT` in the merge request.

Signing message with GPG is a good practice but using "squash commit from
Gitlab" will create commit without signature (it's also the case with merge
commits).

Enforcing only one commit per merge request then allows to not create automatic
squash commits and then have signature of all commits.

template name: `git.max-commit.gitlab-ci.yml`.

Variables:

- `GIT_MAIN_BRANCH`: the main branch of the project (`main` by default)
- `GIT_MAX_COMMIT`: the maximum number of commits allowed to be added (`1` by
  default)

## git signed checker

check that all git commits of the merge request are signed.

template name: `git.signed.gitlab-ci.yml`.

Variables:

- `GIT_MAIN_BRANCH`: the main branch of the project (`main` by default)

## kustomization template

Find all files named `kustomization.yaml` (or `kustomization.yml`) and try
to generate the output in order to check that the file is valid.

Template name: `kustomization.gitlab-ci.yml`
